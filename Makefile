.PHONY: deps build run

deps:
	npm install

build:
	npm run build

subtype: build
	npm run subtype

variance: build
	npm run variance

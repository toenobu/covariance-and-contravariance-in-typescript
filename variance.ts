import { User, Admin} from './user';
import { IsSubType } from './subtype';

// covariance
// true
type T21 = IsSubType<Promise<Admin>, Promise<User>>;

type T22 = IsSubType<'Hello', string>;
// true
type T23 = IsSubType<Capitalize<'Hello'>, Capitalize<string>>;

// contravariance
type Func<Param> = (param: Param) => void;

// false
type T3 = IsSubType<Func<Admin>, Func<User>>;

// true
type T4 = IsSubType<Func<User>, Func<Admin>>;

const logAdmin: Func<Admin> = (admin: Admin): void => {
    console.log(`Name: ${admin.userName}`);
    console.log(`Is super admin: ${admin.isSuperAdmin.toString()}`);
}

const logUser: Func<User> = (user: User): void => {
    console.log(`Name: ${user.userName}`);
}

const logger1: Func<Admin> = logUser;
//const logger2: Func<User> = logAdmin;
import { User, Admin} from './user';

let u = new User("toenobu");
console.log(u);

let a = new Admin("toenobuAd", true);
console.log(a);

export type IsSubType<S, P> = S extends P ? true: false;
type T1 = IsSubType<Admin, User>;

export class User {
    userName: string;

    constructor(userName: string){
        this.userName = userName;
    }
}

export class Admin extends User {
    isSuperAdmin: boolean;
    
    constructor(userName: string, isSuperAdmin: boolean){
        super(userName);
        this.isSuperAdmin = isSuperAdmin;
    }
}